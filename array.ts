/**
 * 复杂数组处理工具类
 * @export
 * @class ArrayTool
 */
export class ArrayTool {

  /**
   * 复杂对象数组，取其中的一个值转换成字符串
   * 
   * @param array 数组
   * @param key 键
   * @param separator 分隔符，默认逗号
   */
  static takeValueToString(array: Array<any>, key: string, separator: string = ','): string {
    let value = '';
    if (array && array.length > 0) {
      array.forEach(v => {
        if (v[key]) { value += v[key] + separator; }
      });
    }
    return value;
  }

  /**
   * 去除空字符串
   * 
   * @param value 字符数组或字符串
   * @param separator 分隔符
   */
  static removeSpace(value: Array<string> | string, separator: any = /[\s|,]/): Array<string> {
    let result = [];
    // 判断是否字符
    if (typeof value === 'string') {
      value = value.split(separator);
    }

    for (let v of value) {
      v = v.trim();
      if (v != '') { result.push(v); }
    }

    return result;
  }
}
