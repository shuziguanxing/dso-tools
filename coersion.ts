/**
 * 转换工具类
 * 
 * @export
 * @class CoersionTool
 */
export class CoersionTool {
  /**
   * 转换成Number类型
   * 
   * @static
   * @param {*} value
   * @param {number} [fallbackValue=0]
   * @returns {number}
   * @memberOf CoersionTool
   */
  static num(value: any, fallbackValue = 0): number {
    return isNaN(parseFloat(value as any)) || isNaN(Number(value)) ? fallbackValue : Number(value);
  }

  /**
   * 转换成Boolean类型
   * 
   * @static
   * @param {*} value
   * @returns {boolean}
   * @memberOf CoersionTool
   */
  static bool(value: any): boolean {
    return value != null && `${value}` !== 'false';
  }
}
