import { isoDateRE, clasicRE, sixtabRE, extendRE } from './regex';

/**
 * crontab 验证工具
 * 
 * @export
 * @class CronTabValidTool
 */
export class CronTabValidTool {

  public static isValid(crontab: string): boolean {
    if (isoDateRE.test(crontab)) return false;
    let tabs = crontab.split(" ");
    let tabsSize = tabs.length;
    if (tabsSize === 5) {
      return this._validateClasic(crontab);
    } else if (tabsSize === 6) {
      return this._validateSix(crontab);
    } else if (tabsSize === 7) {
      return this._validateExtends(crontab);
    }
  }

  private static _validateClasic(crontab): boolean {
    let clasicExec = clasicRE.exec(crontab);
    return clasicExec && clasicExec[0] === crontab;
  }

  private static _validateSix(crontab): boolean {
    let extendExec = sixtabRE.exec(crontab);
    return extendExec && extendExec[0] === crontab;
  }

  private static _validateExtends(crontab): boolean {
    let extendExec = extendRE.exec(crontab);
    return extendExec && extendExec[0] === crontab;
  }
}