import { sixty, hour, dayM, dayW, month, year } from './regexs';

const clasicEx = sixty + " " + hour + " " + dayM + " " + month + " " + dayW;
const extend6_1Ex = sixty + " " + clasicEx;
const extend6_2Ex = clasicEx + " " + year;
const extendEx = sixty + " " + clasicEx + " " + year;

export const isoDateRE = /(\d{4}-[01]\d-[0-3]\dT[0-2]\d:[0-5]\d:[0-5]\d\.\d+[Z]?)|(\d{4}-[01]\d-[0-3]\dT[0-2]\d:[0-5]\d:[0-5]\d)|(\d{4}-[01]\d-[0-3]\dT[0-2]\d:[0-5]\d)|(\d{4}-[01]\d-[0-3]\d)/;
export const clasicRE = new RegExp("^" + clasicEx);
export const extendRE = new RegExp("^" + extendEx);
export const sixtabRE = new RegExp("^((" + extend6_2Ex + ")|(" + extend6_1Ex + "))")