
const alpha = "([0-7])";

const beta = "([*](\/" + alpha + ")?)";

const gamma = "(((-" + alpha + ")?)((\/" + alpha + ")?))";

export const dayW = "((" + alpha + "#" + alpha + ")|(" + alpha + "?(L))|((" + beta + "|(" + alpha + "" + gamma + "))(([,](" + beta + "|(" + alpha + "" + gamma + ")))*)))";