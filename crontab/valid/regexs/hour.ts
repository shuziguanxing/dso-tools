const alpha = "((2[0-3])|([1]?[0-9]))";

const beta = "([*]((\/" + alpha + ")?))";

const gamma = "(" + alpha + "(((-" + alpha + ")?)((\/" + alpha + ")?)))";

export const hour = "((" + beta + "|" + gamma + ")(([,](" + beta + "|" + gamma + "))*))";