const alpha = "([1-5]?[0-9])";
const beta = "([*]((\/" + alpha + ")?))";
const gamma = "(" + alpha + "(((-" + alpha + ")?)((\/" + alpha + ")?)))";

export const sixty = "(" + gamma + "|" + beta + ")(([,](" + gamma + "|" + beta + "))*)";