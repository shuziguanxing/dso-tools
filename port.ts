/**
 * 端口工具类
 */
export class PortTool {
  public static REGEX_PORT: RegExp = /^([0-9]|[1-9]\d{1,3}|[1-5]\d{4}|6[0-5]{2}[0-3][0-5])$/;

  /**
   * 是否端口
   * 
   * @param port 端口
   */
  public static isPort(port: any): boolean {
    return PortTool.REGEX_PORT.test(port);
  }

  /**
   * 是否批量端口字符串
   * 
   * @param ports 待验证字符串
   * @param separator 分隔符，默认分隔空格，逗号，-
   */
  public static isPorts(ports: string, separator: RegExp = /\s|,|-/): boolean {
    let arr = ports.split(separator);
    for (let port of arr) {
      if (!PortTool.isPort(port)) return false;
    }
    return true;
  }

}