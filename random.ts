/**
 * 随机数工具
 * 
 * @export
 * @class RandomTool
 */
export class RandomTool {
  static randomTime(): number {
    return Math.round(new Date().getTime() / 1000);
  }
}
