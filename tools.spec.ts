import { IpTool } from './ip';

describe('Tools', () => {
  it('IpTool fuction isIPV4', () => {
    let result = IpTool.isIPV4('1.1.1.2');
    expect(result).toBe(true);
  });
  it('IpTool fuction getIps', () => {
    let result = IpTool.getIps('192.168.1.1/24');
    expect(result.length).toEqual(254);
  });
});