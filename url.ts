/**
 * URL 工具类
 */
export class UrlTool {
  /* 可包含http|https|ftp|rtsp|mms 可以有参数 */
  public static REGEX_URL: RegExp = /^((https|http|ftp|rtsp|mms)?:\/\/)?(([0-9a-z_!~*'().&=+$%-]+: )?[0-9a-z_!~*'().&=+$%-]+@)?(([0-9]{1,3}\.){3}[0-9]{1,3}|([0-9a-z_!~*'()-]+\.)*([0-9a-z][0-9a-z-]{0,61})?[0-9a-z]\.[a-z]{2,6})(:[0-9]{2,5})?((\/?)|(\/[0-9a-z_!~*'().;?:@&=+$,%#-]+)+\/?)$/i;

  /* 必须包含http|https|ftp|rtsp|mms 可以有参数 */
  public static REGEX_MUST_PROTOCOL_URL: RegExp = /^((https|http|ftp|rtsp|mms)?:\/\/){1}(([0-9a-z_!~*'().&=+$%-]+: )?[0-9a-z_!~*'().&=+$%-]+@)?(([0-9]{1,3}\.){3}[0-9]{1,3}|([0-9a-z_!~*'()-]+\.)*([0-9a-z][0-9a-z-]{0,61})?[0-9a-z]\.[a-z]{2,6})(:[0-9]{2,5})?((\/?)|(\/[0-9a-z_!~*'().;?:@&=+$,%#-]+)+\/?)$/i;

  /* 域名，可包含http|https|ftp */
  public static REGEX_DOMAIN: RegExp = /^(?:(?:(?:https?|ftp):)?\/\/)?(?:\S+(?::\S*)?@)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z0-9]-*)*[a-z0-9]+)(?:\.(?:[a-z0-9]-*)*[a-z0-9]+)*(?:\.(?:[a-z]{2,})).?)(?::\d{2,5})?$/i;

  /* 域名，必须包含http|https|ftp */
  public static REGEX_MUST_PROTOCOL_DOMAIN: RegExp = /^(?:(?:(?:https?|ftp):)\/\/)(?:\S+(?::\S*)?@)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z0-9]-*)*[a-z0-9]+)(?:\.(?:[a-z0-9]-*)*[a-z0-9]+)*(?:\.(?:[a-z]{2,})).?)(?::\d{2,5})?$/i;

  /* 域名，不包含http|https|ftp  */
  public static REGEX_NOT_PROTOCOL_DOMAIN: RegExp = /^(?:\S+(?::\S*)?@)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z0-9]-*)*[a-z0-9]+)(?:\.(?:[a-z0-9]-*)*[a-z0-9]+)*(?:\.(?:[a-z]{2,})).?)(?::\d{2,5})?$/i;

  /**
   * 是否URL
   * 可包含http|https|ftp|rtsp|mms 可以有参数
   * 
   * @param url 
   */
  public static isUrl(url: string): boolean {
    return UrlTool.REGEX_URL.test(url);
  }

  /**
   * 是否URL
   * 必须包含http|https|ftp|rtsp|mms 可以有参数
   * 
   * @param url 
   */
  public static isHasProtocolUrl(url: string): boolean {
    return UrlTool.REGEX_MUST_PROTOCOL_URL.test(url);
  }

  /**
   * 是否域名
   * 可包含http|https|ftp 
   * 
   * @param domain 
   */
  public static isDomain(domain: string): boolean {
    return UrlTool.REGEX_DOMAIN.test(domain);
  }

  /**
   * 是否域名
   * 必须包含http|https|ftp 
   * 
   * @param domain 
   */
  public static isHasProtocolDomain(domain: string): boolean {
    return UrlTool.REGEX_MUST_PROTOCOL_DOMAIN.test(domain);
  }

  /**
   * 是否域名
   * 不包含http|https|ftp 
   * 
   * @param domain 
   */
  public static isNotHasProtocolDomain(domain: string): boolean {
    return UrlTool.REGEX_NOT_PROTOCOL_DOMAIN.test(domain);
  }
}