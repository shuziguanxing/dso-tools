/**
 * 其他验证工具类
 */
export class ValidatorTool {
  /* 邮箱 */
  public static REGEX_EMAIL: RegExp = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;

  /* 手机号码 */
  public static REGEX_MOBILE: RegExp = /^1(3|4|5|7|8)\d{9}$/;

  /* 身份证号码 15位 */
  public static REGEX_ID_CARD_15: RegExp = /^[1-9]\d{7}((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])\d{3}$/;

  /* 身份证号码 18位 */
  public static REGEX_ID_CARD_18: RegExp = /^[1-9]\d{5}[1-9]\d{3}((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])\d{3}([0-9]|X)$/i;

  /* 中文名 2-6位 */
  public static REGEX_C_NAME: RegExp = /^[\u4e00-\u9fa5]{2,6}$/;

  /* 密码复杂度 必须包含字母、数字、特称字符，至少6个字符，最多20个字符 */
  public static REGEX_PASSWORD: RegExp = /^(?=.*[0-9])(?=.*[a-zA-Z])(?=.*[^a-zA-Z0-9]).{6,20}$/;

  /* 邮政编码 */
  public static REGEX_POSTAL: RegExp = /^[1-9]\d{5}$/;

  /* 只包含大小写和数字而不包含特殊字符 */
  public static REGEX_NO_SPECIAL_CHAR: RegExp = /^[a-zA-Z0-9]*$/;

  /* 类似名称、地址等包含中英文、下划线格式的标签内容 */
  public static REGEX_TAG_ZH: RegExp = /^[\\u4E00-\\u9FA5A-Za-z0-9_]+$/;

  /**
   * 是否邮箱
   * 
   * @param email 
   */
  public static isEmail(email: string): boolean {
    return ValidatorTool.REGEX_EMAIL.test(email);
  }

  /**
   * 是否手机号码
   * 
   * @param mobile 
   */
  public static isMobile(mobile: string): boolean {
    return ValidatorTool.REGEX_MOBILE.test(mobile);
  }

  /**
   * 是否身份证号码
   * 
   * @param card 
   */
  public static isIdCard(card: string): boolean {
    return ValidatorTool.REGEX_ID_CARD_15.test(card) || ValidatorTool.REGEX_ID_CARD_18.test(card);
  }

  /**
   * 是否中文名称，且长度2-6
   * @param name 名称
   */
  public static isCName(name: string): boolean {
    return ValidatorTool.REGEX_C_NAME.test(name);
  }

  /**
   * 是否复杂密码
   * 
   * @param password 密码
   */
  public static isComplexPwd(password: string): boolean {
    return ValidatorTool.REGEX_PASSWORD.test(password);
  }

  /**
   * 是否邮政编码
   * 
   * @param value 
   */
  public static isPostal(value: string): boolean {
    return ValidatorTool.REGEX_POSTAL.test(value);
  }

  /**
   * 是否只包含大小写和数字而不包含特殊字符
   * 
   * @param value 
   */
  public static isNoSpecialStr(value: string): boolean {
    return ValidatorTool.REGEX_NO_SPECIAL_CHAR.test(value);
  }

  /**
   * 是否类似名称、地址等包含中英文、下划线格式的标签内容 
   * 
   * @param value 
   */
  public static isTagZH(value: string): boolean {
    return ValidatorTool.REGEX_TAG_ZH.test(value);
  }

}